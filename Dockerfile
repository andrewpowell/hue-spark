FROM java:7
MAINTAINER Andrew Powell <anpowell@deloitte.com>

RUN apt-get update && apt-get -y install git maven npm wget hostname tar git bzip2 unzip && apt-get clean && rm -rf /var/lib/apt/lists/*